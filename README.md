# Download Playlist

## Content

- [Application Overview](#application-overview)
- [Installation](#installation)
- [Requirements](#requirements)
- [Usage](#usage)

### Application Overview

Download Playlist is a straightforward Python script created to make it easy to download multiple videos from a YouTube playlist. The script asks the user for the output folder and playlist URL, then goes ahead and downloads each video in the playlist. It shows the download progress and gives a summary when finished.

### Installation
1. Clone the repository:
```bash
https://gitlab.com/Julien_LAPORTE_IUT/download-playlist.git
```

2. Enter in the clone directory:
```bash
cd download-playlist
```

3. Install the required dependencies:
```bash
pip install -r requirements.txt
```

### Requirements
Make sure you have the following packages installed:
```bash
pip install pytube
```

### Usage
1. Run the script.
2. Enter the output directory.
3. Enter the URL of the YouTube playlist.