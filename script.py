import time
from pytube import Playlist, YouTube
import os
import logging

# @author Julien L.
# @date 01/2024
# Script to download a YouTube playlist

# Check if the URL is a valid YouTube playlist URL
def is_valid_url(url):
    return url.startswith("https://youtube.com/playlist?list=")

# Callback function to display the progress of the download
def on_progress(stream, chunk, bytes_remaining):

    # Calculate the percentage of the download
    percent = round((1 - bytes_remaining / stream.filesize) * 100, 2)

    # Display the progress of the download
    logging.info(f"Download of {stream.title} | {stream.filesize / (1024*1024):.2f} MB | {percent}% complete")

# Download a video from its URL
def download_video(video_url, output_directory):
    try:
        yt = YouTube(video_url)
        output_path = os.path.join(output_directory, f"{yt.title}.mp4")

        # Check if the video already exists in the output directory
        if os.path.exists(output_path):
            logging.info(f"The video {yt.title} already exists in the output directory.")
            return 0
        
        yt.register_on_progress_callback(on_progress)

        # Download the video with the highest resolution
        highest_resolution_stream = yt.streams.get_highest_resolution()

        # Download the video
        highest_resolution_stream.download(output_directory)

        # Return the size of the video
        return highest_resolution_stream.filesize

    except Exception as e:
        logging.error(f"\nError downloading video {video_url}: {e}")
        return 0

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    
    # Ask the user for the output directory
    ask_output_directory = input("Output directory: ")

    # Check if the output directory exists
    if not os.path.exists(ask_output_directory):
        logging.error("The output directory does not exist.")
        exit()

    # Ask the user for the playlist URL
    playlist_url = input("URL of the playlist: ")

    # Check if the URL is valid
    if not is_valid_url(playlist_url):
        logging.error("URL is not valid.")
        exit()

    playlist = Playlist(playlist_url)

    # Create the output directory
    output_directory = f"{ask_output_directory}/{playlist.title}"
    os.makedirs(output_directory, exist_ok=True)

    # Display the number of videos in the playlist
    logging.info(f"Number of videos in playlist {playlist.title}: {len(playlist.video_urls)}\n")

    # Counter for the total size of the playlist
    total_filesize = 0

    # Start the stopwatch
    start_time = time.time()

    for video_url in playlist.video_urls:
        
        # Download the video and get its size
        video_filesize = download_video(video_url, output_directory)
        if video_filesize is not None:
            total_filesize += video_filesize

    # Stop the stopwatch
    end_time = time.time()

    # Display the total size of the playlist and the time taken to download it
    logging.info(f"\nThe total size of the playlist is {total_filesize / (1024*1024):.2f} MB")
    logging.info(f"Time taken to download the playlist: {(end_time - start_time) / 60:.2f} minutes")
    logging.info("\nDownload completed!")